import { MagessPage } from './app.po';

describe('magess App', () => {
  let page: MagessPage;

  beforeEach(() => {
    page = new MagessPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
