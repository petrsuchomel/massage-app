import { NgModule } from '@angular/core';

import { ChartModule } from 'angular2-highcharts';
import { HighchartsStatic } from 'angular2-highcharts/dist/HighchartsService';
import * as highcharts from 'highcharts';

export function highchartsFactory() {
  return highcharts;
}

@NgModule({
  exports: [  
    ChartModule
  ],

  providers: [
    {
      provide: HighchartsStatic,
      useFactory: highchartsFactory
    }
    ]
})
export class HighChartsModule { }