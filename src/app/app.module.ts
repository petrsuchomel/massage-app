import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { routes } from "./routes";

import { HighChartsModule } from './modules/highcharts.module';
import { MaterialModule } from './modules/material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HttpService } from './services/http.service';
import { MainService } from './services/main.service';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { ServicesComponent } from './components/services/services.component';
import { ClientsComponent } from './components/clients/clients.component';
import { ClientComponent } from './components/client/client.component';

import { OverviewComponent } from './components/overview/overview.component';
import { OrderComponent } from './components/order/order.component';

import { DialogComponent } from './components/order/dialog/dialog.component';
import { HomeComponent } from './components/home/home.component';

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyD-UXnBdwUKb8j0UTYbqwG6v4CWXorpVEY",
    authDomain: "ng-app-masaz.firebaseapp.com",
    databaseURL: "https://ng-app-masaz.firebaseio.com",
    projectId: "ng-app-masaz",
    storageBucket: "ng-app-masaz.appspot.com",
    messagingSenderId: "225286839388"
  }
};

@NgModule({
  declarations: [
    AppComponent,
    ServicesComponent,
    ClientsComponent,
    OverviewComponent,DialogComponent,ClientComponent,
    OrderComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    HttpModule,
    MaterialModule,
    HighChartsModule,
    routes,
    AngularFireModule.initializeApp(environment.firebase, 'my-app-name'), // imports firebase/app needed for everything
    AngularFireDatabaseModule, // imports firebase/database, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features
  ],
  providers: [HttpService,MainService],
  entryComponents: [DialogComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
