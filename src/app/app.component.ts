import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase/app';
// import 'hammerjs';
import { MainService } from './services/main.service';

import { FormControl } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Subject } from 'rxjs/Subject'


import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {

  massages: FirebaseListObservable<any>;
  clients: FirebaseListObservable<any>;
  orders: FirebaseListObservable<any>;

  newClientName: string = '';
  newMassageName: string = '';
  newOrderr: string = '';
  // user: Observable<firebase.User>;



  massageId;
  clientId;
  duration;
  income;
  massageName;
  clientName;
  startAt = new Subject()
  endAt = new Subject()
  section;
  selectedUpdate;

  clientbase: any[];
  clientCtrl: FormControl;
  filteredClient: any;


  myControl: FormControl = new FormControl();
  filteredOptions: Observable<any[]>;
  options: any[];

  constructor(private af: AngularFireDatabase) {
    af.list('/clients')
      .subscribe(items => {
        this.options = items;
        this.filteredOptions = this.myControl.valueChanges
          .startWith(null)
          .map(val => val ? this.filter(val) : this.options.slice());
      }
      );
  }

  ngOnInit() {

  }

  filterr() {
    this.filteredOptions = this.myControl.valueChanges
      .startWith(null)
      .map(val => val ? this.filter(val) : this.options.slice());

  }

  filter(val) {
    return this.options.filter(option =>
      option.name.toLowerCase().indexOf(val.toLowerCase()) === 0);
  }


  logout() {
    // this.afAuth.auth.signOut();
  }
  newClient(name: string) {
    this.clients.push({
      id: Date.now(),
      name: this.newClientName,
    });
    this.newClientName = '';
  }
  newMassage() {
    this.massages.push({
      id: Date.now(),
      name: this.newMassageName,
      income: this.income,
      duration: this.duration
    });
  }

  newOrder(name: string) {
    this.orders.push({
      massageId: this.massageId,
      massageName: this.massageName,
      clientId: this.clientId,
      clientName: this.clientName,
      date: Date.now(),
      duration: this.duration,
      income: this.income,
    });
    this.massageId = '';
    this.clientId = '';
    this.duration = '';
    this.income = '';
  }

  update(key, object) {
    if (key == 'client') {
      this.clients.update(object, ({ name: this.newClientName }));
      this.selectedUpdate = 0;
    }
    if (key == 'massage') {
      this.massages.update(object, ({
        name: this.newMassageName,
        income: this.income,
        duration: this.duration
      }));
      this.selectedUpdate = 0;
      this.newMassageName = '';
      this.income = '';
      this.duration = '';
      ;

    }
    if (key == 'order') {
      this.orders.update(object, ({
        massageId: this.massageId,
        massageName: this.massageName,
        clientId: this.clientId,
        clientName: this.clientName,
        date: Date.now(),
        duration: this.duration,
        income: this.income,
      }));
    }
  }

  remove(key, object) {
    if (key == 'massage') {
      this.massages.remove(object);
    }
    if (key == 'client') {
      this.clients.remove(object);
    }
    if (key == 'remove') {
      this.orders.remove(object);
    }
  }

  addValue(key, object) {
    if (key == 'massageId') {
      this.massageId = object.id;
      this.massageName = object.name;
      this.income = object.income;
      this.duration = object.duration;
    }
    if (key == 'clientId') {
      this.clientId = object.id;
      this.clientName = object.name;
    }
    if (key == 'remove') {
      this.orders.remove(object)
    }
    if (key == 'update') {
      this.orders.update(object, ({ date: Date.now() }));
    }
  }

  selectObject(key, object) {
    this.selectedUpdate = object.id;
    if (key == 'massage') {
      this.newMassageName = object.name;
      this.income = object.income;
      this.duration = object.duration;
    }
    if (key == 'client') {
      this.newClientName = object.name;
    }
    if (key == 'order') {
      /*    this.massageId = object.massageId;
         this.massageName = object.massageName;
         this.clientId = object.clientId;
         this.clientName = object.clientName;
         this.duration = object.duration;
         this.income = object.income; */
    }
  }

  openSection(id) {
    if (this.section === id) {
      this.section = 0
    }
    else {
      this.section = id;
    }
  }


}
