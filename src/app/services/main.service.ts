import { Injectable } from '@angular/core';
import { RouterModule, Router } from "@angular/router";
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';


@Injectable()
export class MainService {
  // this.user = this.afAuth.authState;

  clients: FirebaseListObservable<any>;
  massages: FirebaseListObservable<any>;
  orders: FirebaseListObservable<any>;
  constructor(
    public dbAuth: AngularFireAuth,
    private db: AngularFireDatabase,
    private router: Router,
  ) {

    this.orders = this.db.list('/orders', {
      /* query: {
        limitToLast: 50,
      } */
    });

    this.clients = this.db.list('/clients', {
     /*  query: {
        limitToLast: 50,
      } */
    });

    this.massages = this.db.list('/massages', {
      /* query: {
        orderByChild: 'name',
        limitToLast: 6, 
      } */
    });

  }

  getClients() {
    return this.clients;
  }

  getMassages() {
    return this.massages;
  }


  navigate(route) {
    this.router.navigate([route])
  }




}
