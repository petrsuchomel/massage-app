import {Headers, RequestOptions, Response, Http, URLSearchParams} from "@angular/http";
import {Observable, Subject} from "rxjs";
import {Injectable} from "@angular/core";

@Injectable()
export class HttpService {

    private beforeRequest: Subject<String>;
    private afterRequest: Subject<String>;
    static CONTENT_TYPE: Object = {'Content-Type': 'application/json'};

    constructor(private http: Http) {
        this.beforeRequest = new Subject();
        this.afterRequest = new Subject();
    }

    public get(url: string): Observable<any> {
        return this.intercept(this.http.get(url, this.getOptions())
            .map(this.extractData)
            .catch(this.handleError.bind(this)));
    }

    public post(url: string, data: any): Observable<any> {
        return this.intercept(this.http.post(url, data, this.getOptions())
            .map(this.extractData)
            .catch(this.handleError.bind(this)));
    }

    protected intercept(observable: Observable<any>): Observable<Response> {
        this.beforeRequest.next("beforeRequestEvent");
        const afterCallback = () => this.afterRequest.next("afterRequestEvent");
        return observable.do(() => {
        }, afterCallback, afterCallback);
    }

    protected extractData(res: Response) {
        if (res.text().length) {
            return res.json();
        }
        return {};
    }

    protected handleError(error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
            errMsg = `${error.status} - ${error.statusText || ''} ${error.text()}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        if (error.status === 401) {
/*
            this.signIn();
*/
        }
        return Observable.throw(errMsg);
    }

    private getOptions(): RequestOptions {
        const options = new RequestOptions({headers: new Headers(HttpService.CONTENT_TYPE)});
        return options;
    }


}