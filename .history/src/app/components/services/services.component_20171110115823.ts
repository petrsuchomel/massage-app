import { Component, OnInit, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
// import { DialogComponent } from './dialog/dialog.component';

import { MainService } from '../../services/main.service';
import { FormControl, Validators } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialog } from '@angular/material';
@Component({
  selector: 'app-massage',
  templateUrl: './massage.component.html'
})
export class MassageComponent implements OnInit {

  id = 0;
  massage: any;
  massages: any[];
  newMassageName;
  income;
  duration;
  nameControl: FormControl = new FormControl('', [
    Validators.required,]
  );
  durationControl: FormControl = new FormControl('', [
    Validators.required,]
  );
  incomeControl: FormControl = new FormControl('', [
    Validators.required,]
  );

  constructor(



    // public dialog: MatDialog,
    private ms: MainService,
  ) {
    this.ms.getMassages()
      .subscribe(res => {
        this.massages = res;
        console.log(this.massages)
      });
  }

  ngOnInit() {
  }

  edit(m) {
    this.id = m.$key;
    this.newMassageName = m.name;
    this.income = m.income;
    this.duration = m.duration;
  }

  newMassage() {
    this.id = 555;
  }

  deleteMassage(object) {
    this.ms.massages.remove(object);
  }

  discard() {
    this.id = 0;
    this.newMassageName = null;
    this.income = null;
    this.duration = null;
  }

  saveNew() {
    let newMassage = {
      name: this.newMassageName,
      income: this.income,
      duration: this.duration
    }
    this.ms.massages.push(newMassage);
    this.discard()
  }

  saveChanges(object) {
    this.id = 0;
    this.ms.massages.update(object, ({
      name: this.newMassageName,
      income: this.income,
      duration: this.duration
    }));

  }

  /*     update(key, object) {
      if (key == 'client') {
        this.clients.update(object, ({ name: this.newClientName }));
        this.selectedUpdate = 0;
      }
      if (key == 'massage') {
        this.massages.update(object, ({
          name: this.newMassageName,
          income: this.income,
          duration: this.duration
        }));
        this.selectedUpdate = 0;
        this.newMassageName = '';
        this.income = '';
        this.duration = '';
        ;
  
      }
      if (key == 'order') {
        this.orders.update(object, ({
          massageId: this.massageId,
          massageName: this.massageName,
          clientId: this.clientId,
          clientName: this.clientName,
          date: Date.now(),
          duration: this.duration,
          income: this.income,
        }));
      }
    } */


}
