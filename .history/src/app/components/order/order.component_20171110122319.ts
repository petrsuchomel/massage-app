import { Component, OnInit, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { DialogComponent } from './dialog/dialog.component';

import { MainService } from '../../services/main.service';
import { FormControl, Validators } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialog } from '@angular/material';


@Component({
  selector: 'app-order',
  templateUrl: './order.component.html'
})
export class OrderComponent implements OnInit {

  private myDate;
  allSelected: boolean;
  massagesControl: FormControl = new FormControl('', [
    Validators.required,]
  );
  clientsControl: FormControl = new FormControl('', [
    Validators.required,]
  );

  durationControl: FormControl = new FormControl('', [
    Validators.required,]
  );

  incomeControl: FormControl = new FormControl('', [
    Validators.required,]
  );

  dateControl: FormControl = new FormControl('', [
    Validators.required,]

  );



  client: any;
  clients: any[];
  filteredClient: Observable<any>;

  massage: any;
  massages: any[];
  filteredMassage: Observable<any>;

  order;
  orders: any;
  note = "";


  newClientName: string = '';
  newMassageName: string = '';
  newOrderr: string = '';
  // user: Observable<firebase.User>;

  massageId: any;;
  clientId: any;
  duration: any;;
  income: any;;
  massageName: any;;
  clientName: any;;

  section: any;;
  selectedUpdate: any;;

  selection: any;

  setSearchResults(_searchResult: string): void {
  }

  constructor(
    public dialog: MatDialog,
    private ms: MainService,
  ) {
    this.myDate = new Date();

    this.ms.getMassages()
      .subscribe(res => {
        this.massages = res;
        this.filteredMassage = this.massagesControl.valueChanges
          .startWith(null)
          .map(massages => massages && typeof massages === 'object' ?
            (massages.name, this.duration = massages.duration, this.income = massages.income) : massages)
          .map(name => this.filterMassages(name));
      });

    this.ms.getClients()
      .subscribe(res => {
        this.clients = res;
        this.filteredClient = this.clientsControl.valueChanges
          .startWith(null)
          .map(clients => clients && typeof clients === 'object' ? (clients.name) : clients)
          .map(name => this.filterClients(name));
      });

  }

  value() {
    /*   console.log(this.dateControl.value)
        console.log(this.dateControl.value)
          console.log(this.dateControl.value)
     */
    if (typeof this.clientsControl.value === "object") { alert("ss") }
    console.log()
  }

  ngOnInit() { }

  openDialog(): void {
    this.createOrderObj()
    let dialogRef = this.dialog.open(DialogComponent, {
      width: '250px',
      data: {
        data: this.order
      }
    });

    dialogRef.afterClosed().subscribe(res => {
      if (!res) {
        this.controlReset()
      } else {
        this.saveToDatabase()
      }
    });
  }

  createOrderObj() {
    this.order = {
      clientName: this.client.name,
      clientId: this.client.id,
      massageName: this.massage.name,
      massageId: this.massage.id,
      income: this.income,
      duration: this.duration,
      note: this.note,
      date: this.myDate
    }

  }

  controlReset() {
    this.client = this.massage = this.income = this.duration = this.note = this.order = null;
    this.clientsControl.reset();
    this.massagesControl.reset();
    this.clientsControl.reset();
    this.durationControl.reset();
    this.incomeControl.reset();
    this.dateControl.reset();
  }

  saveToDatabase() {
    this.ms.orders.push(this.order);
    this.controlReset();
  }

  displayClient(x): string {
    return x ? x.name : x;
  }

  displayMassage(x): string {
    return x ? x.name : x;
  }

  filterMassages(val: string): string[] {
    return val ? this.massages.filter(s => s.name.toLowerCase().indexOf(val.toLowerCase()) === 0)
      : this.massages;
  }

  filterClients(val: string): string[] {
    return val ? this.clients.filter(s => s.name.toLowerCase().indexOf(val.toLowerCase()) === 0)
      : this.clients;
  }


  btn() {
    this.filteredClient.subscribe(res => console.log(res + "dsexdc"));
    console.log(this.filteredClient)
  }

  newOrder(name: string) {
    this.orders.push({
      massageId: this.massageId,
      massageName: this.massageName,
      clientId: this.clientId,
      clientName: this.clientName,
      date: Date.now(),
      duration: this.duration,
      income: this.income,
    });
    this.massageId = '';
    this.clientId = '';
    this.duration = 0;
    this.income = '';
  }

  addValue(key, object) {
    if (key == 'massageId') {
      this.massageId = object.id;
      this.massageName = object.name;
      this.income = object.income;
      this.duration = object.duration;
    }
    if (key == 'clientId') {
      this.clientId = object.id;
      this.clientName = object.name;
    }
  }
}
