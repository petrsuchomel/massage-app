import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ClientComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
