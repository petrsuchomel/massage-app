import { Component, OnInit, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
// import { DialogComponent } from './dialog/dialog.component';

import { MainService } from '../../services/main.service';
import { FormControl, Validators } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialog } from '@angular/material';
@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html'
})
export class ClientsComponent implements OnInit {


  id = 0;
  client: any;
  clients: any[];
  newclientName;
  income;
  duration;
  nameControl: FormControl = new FormControl('', [
    Validators.required,]
  );
  durationControl: FormControl = new FormControl('', [
    Validators.required,]
  );
  incomeControl: FormControl = new FormControl('', [
    Validators.required,]
  );

  constructor(



    // public dialog: MatDialog,
    private ms: MainService,
  ) {
    this.ms.getClients()
      .subscribe(res => {
        this.clients = res;
        console.log(this.clients)
      });
  }

  ngOnInit() {
  }

  edit(m) {
    this.id = m.$key;
    this.newclientName = m.name;
    this.income = m.income;
    this.duration = m.duration;
  }

  newclient() {
    this.id = 555;
  }

  deleteClient(object) {
    this.ms.clients.remove(object);
  }

  discard() {
    this.id = 0;
    this.newclientName = null;
    this.income = null;
    this.duration = null;
  }

  saveNew() {
    let newclient = {
      name: this.newclientName,
      income: this.income,
      duration: this.duration
    }
    this.ms.clients.push(newclient);
    this.discard()
  }

  saveChanges(object) {
    this.id = 0;
    this.ms.clients.update(object, ({
      name: this.newclientName,
      income: this.income,
      duration: this.duration
    }));

  }

  /*     update(key, object) {
      if (key == 'client') {
        this.clients.update(object, ({ name: this.newClientName }));
        this.selectedUpdate = 0;
      }
      if (key == 'client') {
        this.clients.update(object, ({
          name: this.newclientName,
          income: this.income,
          duration: this.duration
        }));
        this.selectedUpdate = 0;
        this.newclientName = '';
        this.income = '';
        this.duration = '';
        ;

      }
      if (key == 'order') {
        this.orders.update(object, ({
          clientId: this.clientId,
          clientName: this.clientName,
          clientId: this.clientId,
          clientName: this.clientName,
          date: Date.now(),
          duration: this.duration,
          income: this.income,
        }));
      }
    } */


}
