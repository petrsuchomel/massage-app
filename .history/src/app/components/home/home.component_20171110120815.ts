import { Component, OnInit } from '@angular/core';
import { MainService } from '../../services/main.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {
  clients;
  constructor(
    // public dialog: MatDialog,
    private ms: MainService,
  ) {
    this.ms.getClients()
      .subscribe(res => {
        this.clients = res;
        console.log(this.clients)
      });
  }

  ngOnInit() {
  }

}
