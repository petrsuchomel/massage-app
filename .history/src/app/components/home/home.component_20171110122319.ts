import { Component, OnInit } from '@angular/core';
import { MainService } from '../../services/main.service';
import { FormControl, Validators } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {

  clientsControl: FormControl = new FormControl('', [
    Validators.required,]
  );

  client: any;
  clients: any[];
  filteredClient: Observable<any>;
  constructor(
    // public dialog: MatDialog,
    private ms: MainService,
  ) {
    this.ms.getClients()
      .subscribe(res => {
        this.clients = res;
        this.filteredClient = this.clientsControl.valueChanges
          .startWith(null)
          .map(clients => clients && typeof clients === 'object' ? (clients.name) : clients)
          .map(name => this.filterClients(name));
      });
  }

  ngOnInit() {
  }

  filterClients(val: string): string[] {
    return val ? this.clients.filter(s => s.name.toLowerCase().indexOf(val.toLowerCase()) === 0)
      : this.clients;
  }

  displayClient(x): string {
    return x ? x.name : x;
  }

}
