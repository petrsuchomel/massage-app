import { Component, OnInit } from '@angular/core';
import { MainService } from '../../services/main.service';
import { FormControl, Validators } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {
   clientsControl: FormControl = new FormControl('', [
  Validators.required,]
);
  clients;
  constructor(
    // public dialog: MatDialog,
    private ms: MainService,
  ) {
    this.ms.getClients()
      .subscribe(res => {
        this.clients = res;
        console.log(this.clients)
      });
  }

  ngOnInit() {
  }

}
