/**
 * Created by astte on 4/10/17.
 */
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OverviewComponent } from './components/overview/overview.component';
import { ClientsComponent } from './components/clients/clients.component';
import { ClientComponent } from './components/client/client.component';
import { ServicesComponent } from './components/services/services.component';
import { OrderComponent } from './components/order/order.component';
import { HomeComponent } from './components/home/home.component';

export const router: Routes = [

    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: 'home', component: HomeComponent },
    { path: 'overview', component: OverviewComponent },

    { path: 'clients', component: ClientsComponent },
    { path: 'services', component: ServicesComponent },
    { path: 'client', component: ClientComponent },
    { path: 'order', component: OrderComponent },
    { path: '*', redirectTo: '/home', pathMatch: 'full' },



    //   { path: 'land/:id', component: LandComponent },
];

export const routes: ModuleWithProviders = RouterModule.forRoot(router);
