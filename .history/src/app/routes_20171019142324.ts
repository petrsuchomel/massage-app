/**
 * Created by astte on 4/10/17.
 */
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OverviewComponent } from './components/overview/overview.component';
import { ClientComponent } from './components/client/client.component';
import { MassageComponent } from './components/massage/massage.component';
import { OrderComponent } from './components/order/order.component';

export const router: Routes = [

    { path: '', redirectTo: '/order', pathMatch: 'full' },
    { path: 'overview', component: OverviewComponent },
    { path: 'client', component: ClientComponent },
    { path: 'massage', component: MassageComponent },
    { path: 'order', component: OrderComponent },


    //   { path: 'land/:id', component: LandComponent },
];

export const routes: ModuleWithProviders = RouterModule.forRoot(router);
